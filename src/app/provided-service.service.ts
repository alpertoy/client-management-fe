import { URL_BACKEND } from './config/config';
import { Observable } from 'rxjs';
import { ProvidedService } from './provided-service/providedService';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SearchProvidedService } from './provided-service/provided-service-list/searchProvidedService';

@Injectable({
  providedIn: 'root',
})
export class ProvidedServiceService {
  urlBackend = URL_BACKEND + '/api/provided-services';

  constructor(private http: HttpClient) {}

  save(providedService: ProvidedService): Observable<ProvidedService> {
    return this.http.post<ProvidedService>(this.urlBackend, providedService);
  }

  search(name: string, month: number): Observable<SearchProvidedService[]> {

    const httpParams = new HttpParams()
      .set('name', name)
      .set('month', month ? month.toString() : '');

    const url = this.urlBackend + '?' + httpParams.toString();

    // /api/provided/services?name=ASD&month=11
    return this.http.get<any>(url);
  }

}

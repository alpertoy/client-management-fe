import { User } from './user';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  register = false;
  messageSuccess: any = '';
  errors: any = [];


  constructor(
    private router: Router,
    private authService: AuthService) { }

  ngOnInit(): void {

  }

  onSubmit(){
    this.authService
        .login(this.username, this.password)
        .subscribe(response => {
          const access_token = JSON.stringify(response);
          localStorage.setItem('access_token', access_token)
          this.router.navigate(['/home'])
        }, errorResponse => {
          this.errors = ['Username or password incorrect']
        })
  }

  prepareRegister(event: any) {
    event.preventDefault();
    this.register = true;
  }

  cancelRegister() {
    this.register = false;
  }

  registerUser() {
    const user: User = new User();
    user.username = this.username;
    user.password = this.password;
    this.authService
      .save(user)
      .subscribe(response => {
        this.messageSuccess = "Successfully registered! Please login";
        this.register = false;
        this.username = '';
        this.password = '';
        this.errors = []
      }, errorResponse => {
        this.messageSuccess = null;
        this.errors = errorResponse.error.errors;
      });
  }

}

import { SearchProvidedService } from './searchProvidedService';
import { Component, OnInit } from '@angular/core';
import { ProvidedServiceService } from 'src/app/provided-service.service';

@Component({
  selector: 'app-provided-service-list',
  templateUrl: './provided-service-list.component.html',
  styleUrls: ['./provided-service-list.component.css']
})
export class ProvidedServiceListComponent implements OnInit {

  name = '';
  month!: number;
  months: number[];
  list: SearchProvidedService[] = [];
  message: string | undefined | null;

  constructor(private service: ProvidedServiceService) {
    this.months = [1,2,3,4,5,6,7,8,9,10,11,12];
  }

  ngOnInit(): void {
  }

  searchService(){
    this.service.search(this.name, this.month)
      .subscribe(response => {
        this.list = response;
        if (this.list.length <= 0) {
          this.message = 'No Records Found';
        } else {
          this.message = null;
        }
      });
  }

}

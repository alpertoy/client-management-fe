import { Client } from './../../clients/client';
export class SearchProvidedService {
  description!: string;
  value!: number;
  date!: string;
  client!: Client;
}

import { ProvidedServiceService } from './../../provided-service.service';
import { ProvidedService } from './../providedService';
import { ClientsService } from './../../clients.service';
import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/clients/client';

@Component({
  selector: 'app-provided-service-form',
  templateUrl: './provided-service-form.component.html',
  styleUrls: ['./provided-service-form.component.css']
})
export class ProvidedServiceFormComponent implements OnInit {

  clients: Client[] = [];
  providedService: ProvidedService;
  success: boolean = false;
  errors: any = [];

  constructor(private clientService: ClientsService,
    private service: ProvidedServiceService) {
    this.providedService = new ProvidedService();
  }

  ngOnInit(): void {
    this.clientService.getClients().subscribe(
      response => this.clients = response
    );
  }

  onSubmit() {
    this.service.save(this.providedService)
    .subscribe(
        (response) => {
          this.success = true;
          this.errors = null;
          this.providedService = new ProvidedService();
        },
        (errorResponse) => {
          this.success = false;
          this.errors = errorResponse.error.errors;
        }
      );
  }
}

import { Observable } from 'rxjs';
import { User } from './login/user';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_BACKEND, oauthClientId, oauthClientSecret } from './config/config';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  urlBackend = URL_BACKEND + '/api/users';
  tokenUrl = URL_BACKEND + '/oauth/token';
  clientID = oauthClientId;
  clientSecret = oauthClientSecret;
  jwtHelper: JwtHelperService = new JwtHelperService();

  constructor(private http: HttpClient) { }

  getToken() {
    const tokenString = localStorage.getItem('access_token')
    if(tokenString) {
      const token = JSON.parse(tokenString).access_token
      return token;
    }
    return null;
  }

  closeSession() {
    localStorage.removeItem('access_token')
  }

  getAuthenticatedUser() {
    const token = this.getToken();
    if (token) {
      const user = this.jwtHelper.decodeToken(token).user_name
      return user;
    }
    return null;
  }

  isAuthenticated(): boolean {
    const token = this.getToken();
    if (token) {
      const expired = this.jwtHelper.isTokenExpired(token)
      return !expired;
    }
    return false;
  }

  save(user: User): Observable<any> {
    return this.http.post<any>(this.urlBackend, user);
  }

  login(username: string, password: string): Observable<any> {
    const params = new HttpParams()
                      .set('username', username)
                      .set('password', password)
                      .set('grant_type', 'password')

    const headers = {
      'Authorization': 'Basic ' + btoa(`${this.clientID}:${this.clientSecret}`),
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    return this.http.post(this.tokenUrl, params.toString(), { headers });
  }
}

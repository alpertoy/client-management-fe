import { URL_BACKEND } from './config/config';
import { Client } from './clients/client';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  urlBackend = URL_BACKEND + '/api/clients';

  constructor(private http: HttpClient) { }

  save(client: Client): Observable<Client>{
    return this.http.post<Client>(`${this.urlBackend}`, client);
  }

  edit(client: Client): Observable<any>{
    return this.http.put<Client>(`${this.urlBackend}/${client.id}`, client);
  }

  getClients(): Observable<Client[]> {
    return this.http.get<Client[]>(this.urlBackend);
  }

  getClientById(id: number): Observable<Client> {
    return this.http.get<any>(`${this.urlBackend}/${id}`);
  }

  delete(client: Client): Observable<any>{
    return this.http.delete<any>(`${this.urlBackend}/${client.id}`);
  }

}

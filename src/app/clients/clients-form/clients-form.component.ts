import { ClientsService } from './../../clients.service';
import { Client } from './../client';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-clients-form',
  templateUrl: './clients-form.component.html',
  styleUrls: ['./clients-form.component.css'],
})
export class ClientsFormComponent implements OnInit {
  client: Client;
  success = false;
  errors: any = [];
  id!: number;

  constructor(
    private service: ClientsService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.client = new Client();
  }

  ngOnInit(): void {
    let params = this.activatedRoute.params;
    params.subscribe((urlParams) => {
      this.id = urlParams['id'];
      if (this.id) {
        this.service.getClientById(this.id).subscribe(
          (response) => (this.client = response),
          (errorResponse) => (this.client = new Client())
        );
      }
    });
  }

  returnToList() {
    this.router.navigate(['/clients/list']);
  }

  onSubmit() {
    if (this.id) {
      this.service.edit(this.client)
      .subscribe(response => {
        this.success = true;
        this.errors = null;
      }, errorResponse => {
        this.errors = ['Error editing client']
      })
    } else {
      this.service.save(this.client).subscribe(
        (response) => {
          this.success = true;
          this.errors = null;
          this.client = response;
        },
        (errorResponse) => {
          this.success = false;
          this.errors = errorResponse.error.errors;
        }
      );
    }
  }
}

import { ClientsService } from './../../clients.service';
import { Component, OnInit } from '@angular/core';
import { Client } from '../client';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css']
})
export class ClientsListComponent implements OnInit {

  clients: Client[] = [];
  clientSelected!: Client;
  messageSuccess = '';
  messageError = '';

  constructor(
    private service: ClientsService,
    private router: Router) { }

  ngOnInit(): void {
    this.service.getClients()
    .subscribe(response => this.clients = response);
  }

  newRegister() {
    this.router.navigate(['/clients/form'])
  }

  prepareDelete(client: Client) {
    this.clientSelected = client;
  }

  deleteClient() {
    this.service.delete(this.clientSelected)
      .subscribe(response => {
        this.messageSuccess = 'Client has been deleted successfully'
        this.ngOnInit();
      },
      error => this.messageError = 'An error occured while deleting client');
  }

}
